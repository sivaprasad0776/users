const getIndex = (users, id) => {
    let position = -1;
    for (let index = 0; index < users.length; index++) {
        if (users[index].id == id) {
            position = index;
            break;
        }
    }
    return position;
};

const problem1 = function(users, id){
    if( Array.isArray(users) == false ||users.length == 0 || typeof id != 'number'){
        return null;
    }

    const index = getIndex(users, id);

    if (index == -1) {
        return null;
    }
    else {
        return users[index];
    }
};

module.exports = problem1




