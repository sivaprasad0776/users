const problem4 = (users) => {
    if( Array.isArray(users) == false ||users.length == 0){
        return null;
    }

    const emailsList = [];

    for(let index = 0; index < users.length; index++){
        emailsList.push(users[index].email.trim());
    }

    return emailsList;
};

module.exports = problem4;
