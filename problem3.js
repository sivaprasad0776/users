const sortObjs = require('./sortObjs');

const problem3 = (users) => {
    if( Array.isArray(users) == false ||users.length == 0){
        return null;
    }

    const userNameObjects = [];

    for(let index = 0; index < users.length; index++){
        const userName = {
            first_name : users[index].first_name.trim().toUpperCase(),
            last_name : users[index].last_name.trim().toUpperCase()
        };
        userNameObjects.push(userName);
    }

    const sortedUserNameObjects = userNameObjects.sort(sortObjs, prop = 'last_name');
    
    const sortedUserNames = [];

    for(let index = 0; index < sortedUserNameObjects.length; index++){
        const titleCaseFirstName = sortedUserNameObjects[index].first_name[0] + sortedUserNameObjects[index].first_name.slice(1).toLowerCase();
        const titleCaseLastName = sortedUserNameObjects[index].last_name[0] + sortedUserNameObjects[index].last_name.slice(1).toLowerCase();
        const fullName = `${titleCaseFirstName} ${titleCaseLastName}`;
        sortedUserNames.push(fullName);
    }

    return sortedUserNames;

};

module.exports = problem3;
