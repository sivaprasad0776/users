const problem5 = (users) => {
    if (Array.isArray(users) == false || users.length == 0) {
        return null;
    }

    const usersList = [];
    let user = {};

    for (let index = 0; index < users.length; index++) {
        if (users[index].gender.trim().toLowerCase() === "male") {
            user.name = `${users[index].first_name} ${users[index].last_name}`;
            user.email = users[index].email;
            usersList.push(user);
        }
    }

    return usersList;
};

module.exports = problem5;
