let users = require('../users');
const problem2 = require('../problem2');

const result = problem2(users);

let strFormat = "";

if (result != null){
    strFormat = `Last user is ${result.first_name} ${result.last_name} and can be contacted on ${result.email}`;
}
console.log(strFormat);