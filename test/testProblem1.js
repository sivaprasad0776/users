let users = require('../users');
const problem1 = require('../problem1');
const id = 100;

const result = problem1(users, id);

let strFormat = "";

if (result != null){
    strFormat = `${result.first_name} ${result.last_name} is a ${result.gender} and can be contacted on ${result.email}`;
}
console.log(strFormat);