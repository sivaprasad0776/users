const getLastIndex =  (users) => {
    let position = -1
    let maxValue = -1;
    for (let index = 0; index < users.length; index++) {
        if (maxValue < users[index].id) {
            maxValue = users[index].id;
            position = index;
        }
    }
    return position;
};


const problem2 = (users) => {
    if( Array.isArray(users) == false ||users.length == 0){
        return null;
    }
    const lastIndex = getLastIndex(users);
    if (lastIndex == -1) {
        return null;
    }
    else {
        return users[lastIndex];
    }
};

module.exports = problem2;
