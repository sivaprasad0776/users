const problem6 = (users) => {
    if (Array.isArray(users) == false || users.length == 0) {
        return null;
    }
    //Initialising the arrays for each gender Male, Female, Polygender, Bigender, Genderqueer, Genderfluid, Agender
    const usersList = [];
    const maleUsersList = [];
    const femaleUsersList = [];
    const polygenderUsersList = [];
    const bigenderUsersList = [];
    const genderqueerUsersList = [];
    const genderfluidUsersList = [];
    const agenderUsersList = [];
    let gender;


    for (let index = 0; index < users.length; index++) {
        gender = users[index].gender.trim().toLowerCase();

        switch (gender) {
            case 'male':
                maleUsersList.push(users[index]);
                break;
            case 'female':
                femaleUsersList.push(users[index]);
                break;
            case 'polygender':
                polygenderUsersList.push(users[index]);
                break;
            case 'bigender':
                bigenderUsersList.push(users[index]);
                break;
            case 'genderqueer':
                genderqueerUsersList.push(users[index]);
                break;
            case 'genderfluid':
                genderfluidUsersList.push(users[index]);
                break;
            case 'agender':
                agenderUsersList.push(users[index]);
                break;
        }

    }

    usersList.push(maleUsersList);
    usersList.push(femaleUsersList);
    usersList.push(polygenderUsersList);
    usersList.push(bigenderUsersList);
    usersList.push(genderqueerUsersList);
    usersList.push(genderfluidUsersList);
    usersList.push(agenderUsersList);

    return usersList;
};

module.exports = problem6;
